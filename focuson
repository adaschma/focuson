#!/usr/bin/env python3
# Copyright (C) 2018-2019  Adam Schmalhofer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
A CLI to quickly setup your desktop to be optimized for different focuses
e.g. dayjob, personal work and relaxing.

It does this by switching the targets of links and running user provided shell
scripts.
'''

import argparse
import subprocess
from pathlib import Path
from itertools import chain
from collections import namedtuple
import doctest
from os import environ

from debian.deb822 import Deb822
import argcomplete


Alternative = namedtuple('Alternative', ['name', 'prio', 'slaves'])
Slave = namedtuple('Slave', ['name', 'source', 'target'])


class ConfigReader:
    ''' Parse the update-alternatives config file. '''

    def __init__(self, data):
        self.data = data

    @classmethod
    def parse_slaves(cls, paragraph):
        r'''
        >>> para = {'Slaves': '\n gtimelog /home/user/MyProject/gtimelog' +
        ...                   '\n todo.txt /home/user/MyProject/todo-txt'}
        >>> list(ConfigReader.parse_slaves(para))
        ...     #doctest: +NORMALIZE_WHITESPACE
        [['gtimelog', '/home/user/MyProject/gtimelog'],
         ['todo.txt', '/home/user/MyProject/todo-txt']]
        '''
        return (line.strip().split(maxsplit=1)
                for line in paragraph['Slaves'].splitlines()
                if line != '')

    def get_alternative_names(self):
        return (self.get_alternative_name(para) for para in self.data[1:])

    def read(self, name):
        slave_sources = dict(self.parse_slaves(self.data[0]))
        para = [paragraph
                for paragraph in self.data[1:]
                if self.get_alternative_name(paragraph) == name][0]
        slaves = (Slave(s[0], slave_sources[s[0]], s[1])
                  for s in self.parse_slaves(para))
        return Alternative(name, para['Priority'], slaves)

    @classmethod
    def get_alternative_name(cls, paragraph):
        r'''
        >>> para = {'Alternative': '/home/user/.local/share/focuson/default'}
        >>> ConfigReader.get_alternative_name(para)
        'default'
        '''
        return paragraph['Alternative'].split('/')[-1]


class FocusOn:
    '''
    The entry point for running the actual cli commands.

    The `run_`-methods should correspond with the different long arguments of
    the CLI.
    '''

    def __init__(self, home_dir, system=False):
        self.system = system
        self.base_dir = (home_dir / '.local'
                         if not system
                         else Path('/usr/local')
                         ) / 'share/focuson'
        self.alt_dir = self.base_dir / "update-alternatives"
        self.admin_dir = self.alt_dir / "administration"
        self.config_file = self.base_dir / "config"
        self.pre_hook = self.base_dir / "hooks/pre-switch"
        self.post_hook = self.base_dir / "hooks/post-switch"

    def subprocess(self, args, **dic):
        args = (args
                if not self.system
                else ('pkexec',) + args)
        return subprocess.run(args, **dic, check=True)

    def call_update_alternatives(self, *args, **dic):
        return self.subprocess(('update-alternatives',
                                '--altdir', self.alt_dir,
                                '--admindir', self.admin_dir)
                               + args, **dic)

    def run_init(self):
        self.alt_dir.mkdir(parents=True, exist_ok=True)
        self.admin_dir.mkdir(parents=True, exist_ok=True)

    def run_status(self):
        call = self.call_update_alternatives('--query', 'focus',
                                             stdout=subprocess.PIPE,
                                             encoding='utf-8')
        for line in call.stdout.splitlines():
            if line.startswith('Value: '):
                print(line.split('/')[-1])

    def run_display(self):
        self.call_update_alternatives('--display', 'focus')

    def run_list(self):
        self.call_update_alternatives('--list', 'focus')

    def run_interactive(self):
        self.call_update_alternatives('--config', 'focus')

    def run_replace_config(self):
        with self.config_file.open(mode='w') as f:
            self.call_update_alternatives('--query', 'focus', stdout=f)

    def run_edit_config(self):
        editor_bin = environ.get('EDITOR', 'vi')
        subprocess.call((editor_bin, self.config_file))

    def run_install(self, name):
        c = self.read_config()
        names = ([name]
                 if name != 'all'
                 else c.get_alternative_names())
        for n in names:
            with (self.base_dir / n).open('a'):
                pass    # `touche $file`
            self.call_update_alternatives(*self.build_install_args(c.read(n)))

    def run_remove(self, name):
        self.call_update_alternatives(*self.build_remove_args(name))

    def read_config(self):
        with open(self.config_file, encoding='utf-8') as f:
            return ConfigReader(list(Deb822.iter_paragraphs(f)))

    def build_install_args(self, alternative):
        '''
        >>> FocusOn(Path('/home/user')
        ...        ).build_install_args(Alternative('default', 99, [])
        ...                             )      #doctest: +NORMALIZE_WHITESPACE
        ['--install', '/home/user/.local/share/focuson/current', 'focus',
         '/home/user/.local/share/focuson/default', '99']

        >>> FocusOn(Path('/home/user'),
        ...         system=True
        ...         ).build_install_args(Alternative('default', 99, [])
        ...                             )      #doctest: +NORMALIZE_WHITESPACE
        ['--install', '/usr/local/share/focuson/current', 'focus',
         '/usr/local/share/focuson/default', '99']

        >>> alt = Alternative('default', 99,
        ...                   [Slave('gtimelog',
        ...                          '/home/user/.local/share/gtimelog',
        ...                          '/home/user/MyProject/gtimelog')],
        ...                  )
        >>> FocusOn(Path('/home/user')
        ...        ).build_install_args(alt)   #doctest: +NORMALIZE_WHITESPACE
        ['--install', '/home/user/.local/share/focuson/current', 'focus',
         '/home/user/.local/share/focuson/default', '99',
         '--slave',
         '/home/user/.local/share/gtimelog',
         'gtimelog',
         '/home/user/MyProject/gtimelog']

        >>> alt = Alternative('default', 99,
        ...                   [Slave('gtimelog',
        ...                          '/home/user/.local/share/gtimelog',
        ...                          '/home/user/MyProject/gtimelog'),
        ...                    Slave('todo.txt',
        ...                          '/home/user/.todo-txt',
        ...                          '/home/user/MyProject/todo-txt'),
        ...                    ])
        >>> FocusOn(Path('/home/user')
        ...        ).build_install_args(alt)    #doctest: +NORMALIZE_WHITESPACE
        ['--install', '/home/user/.local/share/focuson/current', 'focus',
         '/home/user/.local/share/focuson/default', '99',
         '--slave',
         '/home/user/.local/share/gtimelog',
         'gtimelog',
         '/home/user/MyProject/gtimelog',
         '--slave',
         '/home/user/.todo-txt',
         'todo.txt',
         '/home/user/MyProject/todo-txt']
        '''
        slave_args = [['--slave', s.source, s.name, s.target]
                      for s in alternative.slaves]
        return list(chain(['--install', str(self.base_dir / 'current'),
                           'focus',
                           str(self.base_dir / alternative.name),
                           str(alternative.prio)],
                          chain.from_iterable(slave_args)))

    def build_remove_args(self, name):
        '''
        >>> FocusOn(Path('/home/user')
        ...         ).build_remove_args('default')
        ['--remove', 'focus', '/home/user/.local/share/focuson/default']
        '''
        return ['--remove', 'focus', str(self.base_dir / name)]

    @classmethod
    def run_hook(cls, hook):
        try:
            subprocess.call([hook])
        except FileNotFoundError:
            pass

    def run_switch(self, name, skip_hooks):
        if not skip_hooks:
            self.run_hook(self.pre_hook)
        if name is not None:
            self.call_update_alternatives('--set', 'focus',
                                          self.base_dir / name)
        else:
            self.call_update_alternatives('--auto', 'focus')
        if not skip_hooks:
            self.run_hook(self.post_hook)


def get_arg_parser():
    ''' Return a configured `ArgumentParser` '''

    parser = argparse.ArgumentParser()
    exclusive = parser.add_mutually_exclusive_group()
    exclusive.add_argument('--init', action='store_true')
    exclusive.add_argument('--run-tests', action='store_true',
                           help='run internal unit tests')
    exclusive.add_argument('--status', action='store_true')
    exclusive.add_argument('--display', action='store_true')
    exclusive.add_argument('--list', action='store_true')
    exclusive.add_argument('--interactive', action='store_true')
    exclusive.add_argument('--replace-config', action='store_true')
    exclusive.add_argument('--edit-config', action='store_true')
    exclusive.add_argument('--call-backend-with', nargs='*', type=str,
                           help='directly run update-alternatives with paths'
                           + ' set to values focuson uses')
    exclusive.add_argument('--install', nargs='?')
    exclusive.add_argument('--remove', nargs='?')
    exclusive.add_argument('name',
                           help='switch to focus on "name"', nargs='?')
    parser.add_argument('--skip-hooks', action='store_true')
    parser.add_argument('--system', action='store_true')
    return parser


def main():
    ''' Entry point for the CLI '''

    parser = get_arg_parser()
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    instance = FocusOn(Path.home(), args.system)
    if args.call_backend_with is not None:
        instance.call_update_alternatives(args.call_backend_with)
    elif args.run_tests:
        doctest.testmod(verbose=True)
    elif args.init:
        instance.run_init()
    elif args.status:
        instance.run_status()
    elif args.display:
        instance.run_display()
    elif args.list:
        instance.run_list()
    elif args.interactive:
        instance.run_interactive()
    elif args.replace_config:
        instance.run_replace_config()
    elif args.edit_config:
        instance.run_edit_config()
    elif args.install is not None:
        instance.run_install(args.install)
    elif args.remove is not None:
        instance.run_remove(args.remove)
    else:
        instance.run_switch(args.name, args.skip_hooks)


if __name__ == '__main__':
    main()
