A CLI to quickly setup your desktop to be optimized for different focuses
e.g. day job, personal work and relaxing.

Why
======

Computers are great. Unlike the physical world where a completely different set
of tools are needed to cook, repair/maintain my bike, exercise or sleep I can
do everything on one device. The downside is that we loose out on having a
different room which has everything needed close and everything else out of the
way to not distract.


What does it do?
================

focuson switches the targets of links and runs provided shell scripts when you
tell it that you want to switch to a preconfigured focus-setup.

I use it to switch the timelog.txt file that gtimelog_ uses and which
todo.txt_ file has my currently relevant todos as well as a Desktop-like
folder with the relevant documents.

.. _gtimelog: https//gtimelog.org/
.. _todo.txt: https://todotxt.org/


Requirements
============

Currently focuson uses update-alternatives which is available on all
debian-based distributions, but to my knowledge no where else. Patches for
other backends (e.g. native implementation) are welcome.

It is a CLI implemented in python3_.

.. _python3: https://www.python.org/
